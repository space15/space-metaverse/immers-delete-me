import React from 'react'
import ReactDOM from 'react-dom'
import Layout from '../components/Layout'
import PasswordInput from '../components/PasswordInput'

class Reset extends React.Component {
  render () {
    return (
      <div className='im-tabs_content'>
        <div className='im-formContainer'>
          <form action='/auth/reset' method='post'>
            <div className='im-formTitle'>Please choose a new password:</div>
              <PasswordInput />
            <div className='im-m_t16'>
              <button className='im-button_action im-button_full im-block' type='submit'>
                Change Password
              </button>
            </div>
          </form>
        </div>
      </div>
    )
  }
}

const mountNode = document.getElementById('app')
ReactDOM.render(
  <Layout contentTitle='Password reset'>
    <Reset />
  </Layout>, mountNode)
