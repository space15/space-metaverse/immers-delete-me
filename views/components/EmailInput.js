import React from 'react'

export default function EmailInput () {
  return (
    <div className='im-formInputContainer'>
      <label>E-mail address</label>
      <input
        className='im-formInput'
        type='email' name='email'
        required
        placeholder='email address'
      />
    </div>
  )
}
