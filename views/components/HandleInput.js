import React from 'react'

export default class HandleInput extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      username: props.username || '',
      immer: props.immer || process.env.domain
    }
    this.handleInput = this.handleInput.bind(this);
  }

  handleInput (e) {
    this.setState({ [e.target.name]: e.target.value }, () => {
      this.props.onChange(this.state.username, this.state.immer)
    })
  }

  render () {
    return (
      <>
        <div className='im-formTitle'>Enter your account information to login</div>
        <div className='im-formInputContainer'>
          <label htmlFor='username'>Account Name</label>
          <input
            onChange={this.handleInput}
            id='username' className='im-formInput handle'
            type='text' inputMode='email' name='username'
            placeholder='username'
            required pattern='([A-Za-z0-9-@\.]+(@[A-Za-z0-9-@\.]+)?){3,32}'
            autoCapitalize='off' autoCorrect='off' spellCheck='false'
            title='Letters, numbers, &amp; dashes only, between 3 and 32 characters'
            value={this.state.username}
          />
        </div>
        <div className='im-hide'>
          <label htmlFor='immer' className='home-label'>Home Space:</label>
          <span className='handle-bracket'>[</span>
          <input
            onChange={this.handleInput}
            id='immer' className='im-formInput handle'
            type='text' inputMode='url' name='immer'
            placeholder={process.env.domain}
            required pattern='localhost(:\d+)?|.+\..+'
            autoCapitalize='off' autoCorrect='off' spellCheck='false'
            title='Valid domain name, including .'
            value={this.state.immer} disabled={this.props.lockImmer}
          />
          <span className='handle-bracket'>]</span>
        </div>
      </>
    )
  }

  componentDidUpdate () {
    const { username, immer } = this.props
    if (username !== this.state.username || immer !== this.state.immer) {
      this.setState({ username, immer })
    }
  }
}
